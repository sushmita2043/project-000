Project 002: Electricity Consumption Prediction for residential areas in Indiana
===================================================================================

Participants
============

Abeykoon, Vibhatha, F16-EG-1000, vibhatha, vlabeyko@umail.iu.edu


Ray, Sushmita, F16-DG-4059, sushmita2043, sushray@umail.iu.edu

Abstract
========

Energy consumption has been increasing steadily due to globalization and industrialization. Studies have shown that buildings are responsible for the biggest proportion of energy consumption; for example in European Union countries, energy consumption in buildings represents around 40 percent of the total energy consumption. Our main expectation is to provide an analytical view on the prediction of electrical consumption in the residential areas (residential statistics) of Indiana State.
We analyse the factors affecting the power consumption. Furthermore, the affect of weather, economy and other external and internal factors will be analyzed. 

References
=========

https://gitlab.com/cloudmesh_fall2016/project-002/blob/master/report/references.bib

Deliverables
============

Use subdirectories to cleanly manage your deliverables where appropriate

* Easy **short** instalation instructions
* Easy **short** script that lets us run the application possibly on a subset of 
  data
* Program to fetch the data (data should not relay be stored here, we will stor 
  this elsewhere)
* Program to actually conduct the calculation 
* Report in original format
* Report in PDF
* Images directory with all images included in the report

Recomendations
==============

We recommend that you use sharelatex or edit the report in latex directly in the 
git repository. Make sure your team mambers knwo how to do that. If not indicate 
how you will manage your team report.

Check back with project-000 for tips that we have not yet included here

Changelog
=========

* edit from vm
* change bey another user
